<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function v_register()
    {
        return view('register');
    }

    public function v_welcome(request $request)
    {
        $nama_depan = $request->input('nama_depan');
        $nama_belakang = $request->input('nama_belakang');
        $data = [
            'nama_depan' => $nama_depan,
            'nama_belakang' => $nama_belakang
        ];
        return view('welcome', $data);
    }
}
